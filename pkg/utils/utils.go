package utils

import (
	"fmt"
	"time"
)

func Echo() {
	fmt.Println("Hello, world 222?.")
}

func P(Text any) {
	fmt.Println("[", Now(), "]", Text)
}

func Now() string {

	now := time.Now()
	timeString := fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	return timeString

}
