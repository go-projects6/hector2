package db

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var userCollection *mongo.Collection
var ctx = context.TODO()

func Init() {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}

	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		panic(err)
	}

	userCollection = client.Database("hector").Collection("users")

}

func GetUserById() bson.D {

	filter := bson.D{{"id", 1}}
	var result bson.D

	userCollection.FindOne(ctx, filter).Decode(&result)
	fmt.Println(result)

	return result

}
